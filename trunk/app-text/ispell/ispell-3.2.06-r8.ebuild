# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/app-text/ispell/ispell-3.2.06-r6.ebuild,v 1.16 2007/03/25 06:38:14 grobian Exp $

inherit eutils

PATCH_VER="0.1"
DESCRIPTION="fast screen-oriented spelling checker"
HOMEPAGE="http://fmg-www.cs.ucla.edu/geoff/ispell.html"
SRC_URI="http://fmg-www.cs.ucla.edu/geoff/tars/${P}.tar.gz
	mirror://gentoo/${P}-r6-gentoo-${PATCH_VER}.diff.bz2"

SLOT="0"
LICENSE="as-is"
#KEYWORDS="alpha amd64 hppa mips ppc ppc-macos sparc x86"
KEYWORDS="~x86 ~amd64"
IUSE=""
LINGUAS="bg cs da de es et fi fr ga hu it lt nl no pl pt pt_BR ru sl sv"
for X in ${LINGUAS} ; do # is this correct?
	IUSE="${IUSE} linguas_${X}"
done

DEPEND="sys-apps/sed
	sys-devel/bison
	sys-apps/miscfiles
	>=sys-libs/ncurses-5.2
	app-admin/eselect-ispell"
RDEPEND="linguas_fi? ( app-text/tmispell )"
PDEPEND="linguas_bg? ( app-dicts/ispell-bg )
	linguas_cs? ( app-dicts/ispell-cs )
	linguas_da? ( app-dicts/ispell-da )
	linguas_de? ( app-dicts/ispell-de )
	linguas_es? ( app-dicts/ispell-es )
	linguas_et? ( app-dicts/ispell-et )
	linguas_fr? ( app-dicts/ispell-fr )
	linguas_ga? ( app-dicts/ispell-ga )
	linguas_hu? ( app-dicts/ispell-hu )
	linguas_it? ( app-dicts/ispell-it )
	linguas_lt? ( app-dicts/ispell-lt )
	linguas_nl? ( app-dicts/ispell-nl )
	linguas_no? ( app-dicts/ispell-no )
	linguas_pl? ( app-dicts/ispell-pl )
	linguas_pt? ( app-dicts/ispell-pt )
	linguas_pt_BR? ( app-dicts/ispell-pt )
	linguas_ru? ( app-dicts/ispell-ru )
	linguas_sl? ( app-dicts/ispell-sl )
	linguas_sv? ( app-dicts/ispell-sv )"

src_unpack() {
	unpack ${A}
	epatch "${WORKDIR}"/${P}-r6-gentoo-${PATCH_VER}.diff
	epatch "${FILESDIR}"/${P}-patch.diff
}

src_compile() {
	make config.sh || die

	#Fix config.sh to install to ${D}
	cp -p config.sh config.sh.orig
	sed \
		-e "s:^\(BINDIR='\)\(.*\):\1${D}\2:" \
		-e "s:^\(LIBDIR='\)\(.*\):\1${D}\2:" \
		-e "s:^\(MAN1DIR='\)\(.*\):\1${D}\2:" \
		-e "s:^\(MAN4DIR='\)\(.*\):\1${D}\2:" \
		< config.sh > config.sh.install

	make || die
}

src_install() {
	cp -p  config.sh.install config.sh

	#Need to create the directories to install into
	#before 'make install'. Build environment **doesn't**
	#check for existence and create if not already there.
	dodir /usr/bin /usr/lib/ispell /usr/share/info \
		/usr/share/man/man1 /usr/share/man/man5

	make \
		install || die "install failed"

	mv ${D}/usr/bin/ispell ${D}/usr/bin/ispell.real

	rmdir ${D}/usr/share/man/man5
	rmdir ${D}/usr/share/info

	dodoc Contributors README WISHES

	dosed ${D}/usr/share/man/man1/ispell.1
}

pkg_postinst() {
	elog "ispell was installed under ${ROOT}/usr/bin/ispell.real"
	eselect ispell update --if-unset
	elog "You can manage ${ROOT}/usr/bin/ispell implementations with:"
	elog "  eselect ispell"
	if use linguas_fi ; then
		elog "Using tmispell instead of ispell_fi"
	fi
}

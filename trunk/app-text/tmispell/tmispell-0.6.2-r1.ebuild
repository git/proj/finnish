# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils

MY_P=${PN}-voikko-${PV}

DESCRIPTION="Tmispell is interface between ispell OO.o Finnish spellcheckers."
SRC_URI="mirror://sourceforge/voikko/${MY_P}.tar.gz"
HOMEPAGE="http://voikko.sf.net/"
IUSE="enchant nls"

SLOT="0"
LICENSE="GPL-2"
KEYWORDS="~x86 ~amd64"

DEPEND=">=app-text/ispell-3.2.06-r7
	!app-dicts/ispell-fi
	>=app-text/voikko-1.0
	>=dev-libs/glib-2.0.0
	nls? ( sys-devel/gettext )
	sys-apps/coreutils
	sys-devel/bison
	sys-apps/miscfiles
	>=sys-libs/ncurses-5.2
	dev-util/pkgconfig
	app-admin/eselect-ispell"
RDEPEND=">=app-text/ispell-3.2.06-r7
	>=app-text/voikko-1.0
	>=dev-libs/glib-2.0.0
	enchant? ( >=app-text/enchant-1.1.6 )
	nls? ( sys-devel/gettext )"

S="${WORKDIR}/${MY_P}"

pkg_setup() {
	if [[ -d "${ROOT}/usr/lib/ispell" ]] ; then
		ISPELL_DIR=/usr/lib/ispell
	elif [[ -d "${ROOT}/usr/share/ispell" ]] ; then
		ISPELL_DIR=/usr/share/ispell
	else
		die "Couldn’t find ispell data directory from root ${ROOT}"
	fi
}

src_compile() {
	econf $(use_enable enchant ) || die "econf failed"
	emake || die "emake failed"
}

src_install() {
	emake DESTDIR="${D}" install || die "install failed"
	dodoc AUTHORS ChangeLog LUEMINUT NEWS README tmispell.conf.example ||\
		die "docs missing"
	insinto /etc/
	newins "${FILESDIR}/tmispell.conf.gentoo" tmispell.conf
	dodir ${ISPELL_DIR}
	touch "${D}/${ISPELL_DIR}/"{suomi,finnish}.{hash,aff}
}

pkg_postinst() {
	elog "Use “eselect ispell set tmispell” to get tmispell to work with"
	elog "legacy software that expects ispell"
	eselect ispell update --if-unset
}

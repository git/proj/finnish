# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit subversion autotools

DESCRIPTION="Free Finnish spellchecking and huphenation library"
HOMEPAGE="http://voikko.sourceforge.net/"
ESVN_REPO_URI="https://voikko.svn.sourceforge.net/svnroot/voikko/trunk/libvoikko"
#ESVN_BOOTSTRAP="autogen.sh"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="-x86 -amd64" # Live SVN not for public consumption
IUSE="hfst"

DEPEND="sci-misc/malaga
	!hfst? ( =sci-misc/suomi-malaga-9999 )
	virtual/libiconv
	hfst? ( sci-misc/hfst )"
	#hfst? ( sci-misc/omorfi )
RDEPEND="${DEPEND}"

src_unpack() {
	subversion_src_unpack
	cd "${S}"
	eautoreconf
}

src_compile() {
	econf CXXFLAGS="-Wno-error -Wno-deprecated" $(use_enable hfst) \
		|| die "econf failed"
	emake || die "emake failed"
}

src_install() {
	emake DESTDIR="${D}" install || die "install failed"
	dodoc README || die "docs missing"
}


# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils

DESCRIPTION="Finnish text indexing tool"
HOMEPAGE="http://joyds1.joensuu.fi/sukija/sukija.html"
SRC_URI="http://joyds1.joensuu.fi/sukija/${P}.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="-x86" # flex borkage, CXX?
IUSE="qt doc boost"

DEPEND="=sci-misc/suomi-malaga-0.9*
	|| ( sys-devel/bison dev-util/yacc )
	boost? ( >=dev-libs/boost-1.34 )
	sys-libs/db
	x11-libs/qt
	doc? ( app-doc/doxygen )"
RDEPEND="=sci-misc/suomi-malaga-0.9*
	boost? ( >=dev-libs/boost-1.34 )
	sys-libs/db
	x11-libs/qt"

pkg_setup() {
	if built_with_use sys-libs/db nocxx; then
		die "sys-libs/db must be built with USE=-nocxx for sukija to work"
	fi
}

src_compile() {
	econf $(use_with boost ) $(use_with qt x ) || die "configure failed"
	emake || die "make failed"
	if use doc ; then
		doxygen doc/Doxyfile
	fi
}

src_install() {
	emake DESTDIR="${D}" install || die "make install failed"
	dodoc AUTHORS ILMOITUS INSTALL LUE.MINUT NEWS README || die "docs missing"
	dohtml sukija.html || die "docs missing"
	insinto /usr/share/doc/${PF}
	doins kuva1.png malaga{1,2}.png || die "docs missing"
	if use doc ; then
		doins -r doc
	fi
	insinto /usr/share/${PN}
	doins init/rc.*
}

pkg_postinst() {
	elog "sample configurations are in ${ROOT}/usr/share/${PN},"
	elog "you should copy one in to ~/.sukija/ before use"
}

# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils

DESCRIPTION="Finnish diphones and text to speech script for festival"
HOMEPAGE="http://www.ling.helsinki.fi/suopuhe"
SRC_URI="http://www.ling.helsinki.fi/suopuhe/download/hy_fi_mv_diphone-${PV}.tgz
	http://phon.joensuu.fi/suopuhe/tulosaineisto/suo_fi_lj-1.0g-20051204.tgz
	http://www.ling.helsinki.fi/suopuhe/download/lavennin-${PV}.tgz"

LICENSE="GPL-2 LGPL-2"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

DEPEND="app-accessibility/festival"
RDEPEND="dev-lang/perl"

src_unpack() {
	unpack ${A}
	cd "${WORKDIR}/lavennin/bin/"
	sed -i \
		"s:my \$DATA_DIR = \$HOME . \"/data\":my \$DATA_DIR = ${ROOT}/usr/share/suopuhe/data:" \
		lavennin
}

src_install() {
	cd "${WORKDIR}"
	dodoc festival/lib/voices/finnish/hy_fi_mv_diphone/README.mv
	rm festival/lib/voices/finnish/hy_fi_mv_diphone/{README.mv,LICENSE}
	insinto /usr/share/festival/
	cd festival/lib/
	doins -r voices/
	cd "${WORKDIR}/lavennin/"
	newdoc README.txt README.lavennin
	dodoc man/*.shtml
	newbin bin/lavennin suopuhe-lavennin
	dodir /usr/share/suopuhe/data/
	insinto /usr/share/suopuhe
	doins -r data
}

pkg_postinst() {
	elog "TTS perl script installed as suopuhe-lavennin"
}


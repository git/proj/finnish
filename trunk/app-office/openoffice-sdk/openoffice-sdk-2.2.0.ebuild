# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit java-pkg-opt-2

OOI_SCRIPT="OOo-SDK_2.2.0_LinuxIntel_install.sh"
DESCRIPTION="OO.o SDK and other binaries missing from Gentoo’s OO.o"
HOMEPAGE="http://download.openoffice.org/2.2.0/sdk.html"
SRC_URI="http://mirrors.dotsrc.org/openoffice/stable/${PV}/${OOI_SCRIPT}
ftp://rpmfind.net/linux/fedora/core/development/i386/os/Fedora/openoffice.org-core-${PV}-14.6.i386.rpm
"

LICENSE="LGPL-2"
SLOT="0"
KEYWORDS="~x86"

QA_EXECSTACK="usr/lib/openoffice/linux/bin/idlcpp
usr/lib/openoffice/linux/bin/regcomp.bin
usr/lib/openoffice/linux/bin/cppumaker
usr/lib/openoffice/linux/bin/idlc
usr/lib/openoffice/linux/bin/regcompare
usr/lib/openoffice/linux/bin/regview
usr/lib/openoffice/linux/bin/uno-skeletonmaker
usr/lib/openoffice/linux/bin/rdbmaker
usr/lib/openoffice/linux/bin/javamaker
usr/lib/openoffice/linux/bin/xml2cmp
usr/lib/openoffice/linux/bin/sp2bv
usr/lib/openoffice/linux/bin/unoapploader
usr/lib/openoffice/linux/bin/regmerge
usr/lib/openoffice/linux/bin/autodoc" #dodgybinarieswithexecstacks,meh

DEPEND="|| ( =app-office/openoffice-2.2* =app-office/openoffice-bin-2.2* )
	sys-apps/sed
	>app-arch/zip-2.3
	|| ( app-arch/rpm2targz )
	"
RDEPEND="|| ( =app-office/openoffice-2.2* =app-office/openoffice-bin-2.2* )
	>app-arch/zip-2.3
	"

src_unpack() {
	mkdir "${S}"
	cd "${S}"
	# check line number from sh on update
	tail --lines=+88 "${DISTDIR}/${OOI_SCRIPT}" | tar xf -
	assert
	cd RPMS
	rpm2targz openoffice.org-sdk-${PV}-*.i586.rpm || \
		die "Unable to unrpm"
	tar zxpf openoffice.org-sdk-${PV}-*.i586.tar.gz || \
		die "Unable to untar"
	cd opt/openoffice.org2.2_sdk
	sed -e "s:@OO_SDK_HOME@:${ROOT}/usr/lib/openoffice:g" \
		-e "s:@OFFICE_HOME@:${ROOT}/usr/lib/openoffice:g" \
		-e "s:@OO_SDK_ZIP_HOME@:${ROOT}/usr/bin:g" \
		-e "s:@OO_SDK_CPP_HOME@:${ROOT}/usr/bin:g" \
		-e "s:@OO_SDK_MAKE_HOME@:${ROOT}/usr/bin:g" \
		-e "s:@OO_SDK_JAVA_HOME@:${JAVA_HOME}:g" \
		-e "s:/usr/share/automake:${ROOT}/usr/share/automake:g" \
		"${FILESDIR}/setsdkenv_unix.sh.gentoo" > setsdkenv_unix.sh
	sed -e "s:@OO_SDK_HOME@:${ROOT}/usr/lib/openoffice:g" \
		-e "s:@OFFICE_HOME@:${ROOT}/usr/lib/openoffice:g" \
		-e "s:@OO_SDK_ZIP_HOME@:${ROOT}/usr/bin:g" \
		-e "s:@OO_SDK_CPP_HOME@:${ROOT}/usr/bin:g" \
		-e "s:@OO_SDK_MAKE_HOME@:${ROOT}/usr/bin:g" \
		-e "s:@OO_SDK_JAVA_HOME@:${JAVA_HOME}:g" \
		-e "s:/usr/share/automake:${ROOT}/usr/share/automake:g" \
		"${FILESDIR}/setsdkenv_unix.csh.gentoo" > setsdkenv_unix.csh
	cd "${S}"
	rpm2targz "${DISTDIR}/openoffice.org-core*rpm" || \
		die "unable to unrpm"
	tar zxpf openoffice.org-cor*tar.gz || die "unable to untar"
}

src_install() {
	# tools, examples & api docs
	dodir /usr/lib/openoffice
	insinto /usr/lib/openoffice
	cd "${S}/RPMS/opt/openoffice.org2.2_sdk/"
	doins -r classes examples idl linux settings xml include
	doins config.*
	for f in config.* linux/bin/* ; do
		fperms a+x /usr/lib/openoffice/${f} || die "fperms failed"
	done

	# docs
	dodoc index.html README{,.html}
	dodir /usr/share/doc/${PF}
	insinto /usr/share/doc/${PF}
	doins -r docs

	# SDK env.
	insinto /usr/lib/openoffice/program/
	doins setsdkenv_unix* configure.pl
	for f in setsdkenv_unix{,.sh,.csh} ; do
		fperms a+x /usr/lib/openoffice/program/${f} || die "fperms failed"
	done
	# Patching missing libstlport_gcc
	cd "${S}"
	doins usr/lib/openoffice.org/program/libstlport_gcc.so || die \
		"installing libstrlport_gcc.so failed"
}

pkg_postinst() {
	elog "SDK environment variable setup scripts were installed in: "
	elog "	${ROOT}/usr/lib/openoffice/program"
	elog "Please note that this ugly kludge of package is only used until "
	elog "	http://bugs.gentoo.org/show_bug.cgi?id=151798"
	elog "has been resolved by kind openoffice team"
	ewarn "The binaries here only work every once in a blue moon, sorry!"
}


# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils versionator subversion

DESCRIPTION="Voikko—Free Finnish spell checking and hyphenation for OpenOffice"
HOMEPAGE="http://www.lemi.fi/voikko/"
LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="-x86" # Live SVN not for public consumption
ESVN_REPO_URI="https://svn.sourceforge.net/svnroot/voikko/trunk/ooovoikko"

DEPEND="|| ( >=app-office/openoffice-2 >=app-office/openoffice-bin-2 )
	app-text/voikko
	sys-apps/grep
	sys-apps/sed
	app-office/openoffice-sdk
	!app-office/oo2-voikko"
#  ! app-office/oo2-soikko"
RDEPEND="|| ( >=app-office/openoffice-2 >=app-office/openoffice-bin-2 )
	app-text/voikko"

UNONAME=${PN/office-voikko/office.org-voikko}

pkg_setup() {
	if pidof soffice.bin >/dev/null; then
		die "${PN} can’t be installed while OpenOffice.org is running."
		# Or use the force
		#killall soffice.bin || killall -9 soffice.bin
	fi
	if [[ ! -x "${ROOT}/usr/lib/openoffice/program/setsdkenv_unix.sh" ]] ; then
		die "Couldn’t find open office SDK settings from \
			${ROOT}/usr/lib/openoffice/program/setsdkenv_unix.sh"
	fi

	if has_version app-office/openoffice ; then
		if ! built_with_use app-office/openoffice odk ; then
			ewarn "OpenOffice should be built with USE flag “odk”"
			ewarn "for building uno package from source to work"
		fi
	fi
}

src_compile() {
	source ${ROOT}/usr/lib/openoffice/program/setsdkenv_unix.sh
	echo "OO_SDK_HOME=${OO_SDK_HOME}"
	cd "${WORKDIR}/unopkg"
		# Variables needed to "export" are used in the *.mk files included
#		OO_SDK_HOME=${OO_SDK_HOME} OFFICE_HOME=${OFFICE_HOME} \
#			OO_SDK_NAME=${OO_SDK_NAME} \
#			OO_SDK_MAKE_HOME=${OO_SDK_MAKE_HOME} \
#			OO_SDK_ZIP_HOME=${OO_SDK_ZIP_HOME} \
#			OO_SDK_CPP_HOME=${OO_SDK_CPP_HOME} \
#			OO_SDK_JAVA_HOME=${OO_SDK_JAVA_HOME} \
#			OO_SDK_OUTPUT_DIR=${OO_SDK_OUTPUT_DIR} \
#			SDK_AUTO_DEPLOYMENT=${SDK_AUTO_DEPLOYMENT} \
#			LD_LIBRARY_PATH=${LD_LIBRARY_PATH} 	PATH=${PATH} \
#			OFFICE_PROGRAM_PATH=${OFFICE_PROGRAM_PATH} \
#			SDK_GXX_INCLUDE_PATH=${SDK_GXX_INCLUDE_PATH} \
			emake || die "make uno package failed"
	mkdir "${S}"
	cp build/${UNONAME}-*uno.pkg impl/doc/Linux_*/ASENNUS.txt \
		linux-installer "${S}"
}

src_install() {
	cd "${S}"
	dodoc ASENNUS.txt
	insinto /usr/lib/openoffice/share/
	doins linux-installer
	dodir /usr/lib/${P}
	insinto /usr/lib/${P}
	doins oo2-voikko-Linux*-${PV}.uno.pkg
}

find_unopkg() {
	if has_version '>=app-office/openoffice-bin'; then
		if use amd64 ; then
			UNOPKG="/usr/lib32/openoffice/program/unopkg"
		else
			UNOPKG="/usr/lib/openoffice/program/unopkg"
		fi
	else
		UNOPKG="/usr/lib/openoffice/program/unopkg" #verify!
	fi
}

pkg_postinst() {
	# N.B.: uno packages meddle with $HOME, let's fool it
	HOME="${S}"
	cd "${S}"
	# Yay, OO.o package installation requires .ooo-2.0 directory from once
	# started instance of the program. How fun!
	tar jxf "${FILESDIR}/ooo-bootstrap-decoy.tar.bz2"

	find_unopkg

	# select component
	if use amd64; then
		COMPONENT="${ROOT}/usr/lib/${P}/${UNONAME}-Linux_x86_64-${PV}.uno.pkg"
	else
		COMPONENT="${ROOT}/usr/lib/${P}/${UNONAME}-Linux_x86-${PV}.uno.pkg"
	fi
	PKG=$(basename "${COMPONENT}")

	# Global scope in install-oo2-voikko
	if [[ -e "${HOME}/.openoffice.org2/.lock" ]] ;
	then
		die "lockfile from previous failed installation attempt found in \
			${HOME}/.openoffice.org2/.lock, please clean and retry"
	fi
	# register()
	if [[ "${COMPONENT}" == "" ]] ;
	then
		die "UNO component not found"
	fi
	einfo "Trying to register uno package ${COMPONENT}..."
	HOME=${HOME} "${UNOPKG}" add --shared "${COMPONENT}"
	if [[ $? == 0 ]] ;
	then
		einfo "Voikko package (${PKG}) registered succesfully"
	else
		die "Couldn't register ${PN} package (${PKG})"
	fi
	elog "Please note that ${PN} is very dependent on ABI compatible "
	elog "version of OpenOffice.org to exist on system when removing voikko!"
	elog "Before incompatible update or removal of OpenOffice you must"
	elog "unmerge ${PN}!"
}

pkg_prerm() {
	find_unopkg
	#unregister()
	UNOPKG_LIST="$(${UNOPKG} list --shared 2> /dev/null)"
	if [[ $? != 0 ]] ; then
		ewarn "Couldn't list existing packages..."
	fi
	PKG=$(echo "${UNOPKG_LIST}" | egrep -m1 "^Name: ${UNONAME}-Linux_x86(_64)?-${PV}" | sed -e "s/Name: //")
	if [ "${PKG}" != "" ]; then
		einfo "Removing uno package ${PKG}..."
		if "${UNOPKG}" remove --shared ${PKG} &>/dev/null
		then
			einfo "${PN} package (${PKG}) unregistered succesfully"
		else
			die "Couldn't uninstall existing ${PN} packages"
		fi
	else
		ewarn "Couldn't find existing ${PN} packages."
		ewarn "You may need to \`unopkg remove\` them manually."
	fi
}


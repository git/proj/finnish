# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

DESCRIPTION="Manages the /usr/bin/ispell symlink"
HOMEPAGE="http://overlays.gentoo.org/proj/finnish/"
SRC_URI="http://dev.gentoo.org/~flammie/ispell.eselect-${PVR}.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

RDEPEND=">=app-admin/eselect-1.0.6"

src_install() {
	insinto /usr/share/eselect/modules
	newins "${WORKDIR}/ispell.eselect-${PVR}" ispell.eselect || die
}

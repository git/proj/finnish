# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit subversion

DESCRIPTION="Finnish wordform patterns for malaga"
HOMEPAGE="http://joyds1.joensuu.fi/sukija/sukija.html"
ESVN_REPO_URI="https://voikko.svn.sourceforge.net/svnroot/voikko/trunk/suomimalaga"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="-x86 -amd64" # Live SVN is not for general consumption
IUSE="doc"

DEPEND=">=sci-misc/malaga-7.8
	doc? ( dev-tex/hevea )
	sys-apps/grep
	sys-apps/sed"
RDEPEND="sci-misc/malaga"

src_compile() {
	einfo "Making only voikko..."
	emake voikko || die "emake failed"
}

src_install() {
	emake DESTDIR="${D}"/usr/lib/voikko/ voikko-install || die "install
		failed"
	dodoc ChangeLog LUE.MINUT README
}

# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="2"

MY_P=${P/_rc/rc}

DESCRIPTION="Mozilla plugin for voikko spellchecker"
HOMEPAGE="http://voikko.sf.net"
#SRC_URI="http://www.puimula.org/htp/testing/${MY_P}.tar.gz"
SRC_URI="mirror://sourceforge/voikko/${MY_P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

DEPEND="app-text/voikko
	net-libs/xulrunner"
RDEPEND="${DEPEND}"

S="${WORKDIR}/${MY_P/rc?/}"

src_prepare() {
	sed -i -e 's/-Werror//' -e 's/libxul-unstable/libxul/' \
		Makefile* src/Makefile*
}

src_compile() {
	# only xulrunner build is supported for distributions
	emake -f Makefile.xulrunner\
		XULRUNNER_SDK=/usr/include/xulrunner-$(best_version \
		net-libs/xulrunner)/ || die "emake failed"
}


src_install() {
	emake -f Makefile.xulrunner\
		DESTDIR="${D}/usr/lib/mozilla-firefox/extensions/" \
		install-unpacked || die "install failed"
	dodoc README README.xulrunner ChangeLog || die "docs missing"
}
